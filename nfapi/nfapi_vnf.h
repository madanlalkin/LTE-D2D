#if !defined(NFAPI_VNF_H__)
#define NFAPI_VNF_H__

void configure_nfapi_vnf(char *vnf_addr, int vnf_p5_port);

#endif
