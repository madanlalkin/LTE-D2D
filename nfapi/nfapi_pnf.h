#if !defined(NFAPI_PNF_H__)
#define NFAPI_PNF_H__

void configure_nfapi_pnf(char *vnf_ip_addr, int vnf_p5_port, char *pnf_ip_addr, int pnf_p7_port, int vnf_p7_port);

#endif
